/**
 * Created by roque on 07/12/2018.
 */
let page_sum = require('../Pages/page_sum');

describe('QUESTÂO 01 - Quality Assurance [DevOps]', function(){
    it("CT01 - ", function() {
        const a = 10;
        const b = 30;
        const data = page_sum.ways(a, b);
        expect(data).toEqual(['truco', 'corre', 'rodada encerrada']);
    });
    it("CT02 - ", function() {
        const a = 2;
        const b = 4;
        const data = page_sum.ways(a, b);
        expect(data).toEqual(['truco', 'seis', 'rodada encerrada']);
    });
    it("CT03 - ", function() {
        const a = 0;
        const b = 0;
        const data = page_sum.ways(a, b);
        expect(data).toEqual(['seis', 'rodada encerrada']);
    });
    it("CT03 - ", function() {
        const a = 30;
        const b = 10;
        const data = page_sum.ways(a, b);
        expect(data).toEqual(['corre', 'rodada encerrada']);
    });

});

describe('QUESTÂO 02 - Quality Assurance [DevOps]', function(){
    it("SUM - ", function() {
        const array = [ 2, 5, 1, 7, 2, 0, 12, 34, 56, 3, 10 ];
        const result = page_sum.calc(array);
        expect(result).toEqual({ maior: 56, media: 12, soma: 132 });
    });
});
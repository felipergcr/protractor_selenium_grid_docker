/**
 * Created by roque on 07/12/2018.
 */
let page_softplan = require('../Pages/page_softplan');

describe('QUESTÂO 07 - Quality Assurance [DevOps]', function() {
    beforeEach(function (done) {
        originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 1000000;
        setTimeout(function () {
            browser.ignoreSynchronization = true;
            browser.waitForAngular();
            browser.get('http://automationpractice.com/index.php?');
            done();
        }, 100);
    });
    afterEach(function() {
        browser.restart();
    });

    afterAll(function(){
        browser.close();
    });

    it("CT01 - PESQUISA PRODUTOS EXISTENTES", function() {
        page_softplan.waitScreenHome();
        page_softplan.inputPesquisa('Blouse');
        page_softplan.clickButtonPesquisa();
        page_softplan.clickLinkBlouse();
        page_softplan.waitProductBlouse();
        page_softplan.clickPhotos();
        page_softplan.waitEnlargedPhoto();
        const result = page_softplan.message();

        expect(result.getText()).toEqual('Blouse');
    });

    it("CT02 - PESQUISA PRODUTOS NÃO EXISTENTES", function() {
        page_softplan.waitScreenHome();
        page_softplan.inputPesquisa('ProdutoNãoExiste');
        page_softplan.clickButtonPesquisa();
        const result = page_softplan.messageError();

        expect(result.getText()).toEqual('No results were found for your search "ProdutoNãoExiste"');
    });
});
/**
 * Created by roque on 07/12/2018.
 */
let page_sum = function() {

    this.ways = function(a, b) {
        const result = [];
        if (b > a) {
            result.push('truco');
        }
        if (b === 2*a) {
            result.push('seis');
        }
        else {
            result.push('corre');
        }
        result.push('rodada encerrada');
        return result;
    };


    this.calc = function(array) {
        let maior = null;
        let menor = array[0];
        let soma = 0;
        let media = 0;
        const tamanho = array.length;
        let iguais = [];
        for (const a of array) {
            if (a > maior) {
                maior = a;
            }
            if (a < menor) {
                menor = a;
            }
            soma += a;
        }
        media = soma / tamanho;
        const resultado = {
            maior,
            media,
            soma,
        };
        return resultado;

    };
};
module.exports = new page_sum();
/**
 * Created by roque on 07/12/2018.
 */
let page_softplan = function() {
    this.waitScreenHome = function(){

        var EC = protractor.ExpectedConditions;
        let result = browser.driver.wait(() => {
            return true;
        });
        result.then((value) => {
            let home = element(by.id('header_logo'));
            browser.wait(EC.visibilityOf(home), 5000);
        }).catch('Erro');
    };
    this.clickButtonPesquisa = function() {
        element(by.xpath('.//*[@class="btn btn-default button-search"]')).click();
    };
    this.clickLinkBlouse = function() {
        element(by.xpath('.//*[@class="product-image-container"]')).click();
    };
    this.waitProductBlouse = function() {
        var EC = protractor.ExpectedConditions;
        let result = browser.driver.wait(() => {
            return true;
        });
        result.then((value) => {
            let blouse = element(by.id('image-block'));
            browser.wait(EC.visibilityOf(blouse), 5000);
        }).catch('Erro');
    };
    this.clickPhotos = function() {
        element(by.xpath('.//*[@class="span_link no-print"]')).click();

    };
    this.waitEnlargedPhoto = function() {
        var EC = protractor.ExpectedConditions;
        let result = browser.driver.wait(() => {
            return true;
        });
        result.then(() => {
            const text = element(by.xpath('.//*[@class="child"]'));
            browser.wait(EC.visibilityOf(text), 5000);
        }).catch('Erro');
    };
    this.inputPesquisa = function(text) {
        element(by.id('search_query_top')).clear();
        element(by.id('search_query_top')).sendKeys(text);
    };
    this.message = function() {
        const message = element(by.xpath('.//*[@class="child"]'));

        return message.getText();
    };
    this.messageError = function() {
        const message = element(by.xpath('.//*[@class="alert alert-warning"]'));

        return message.getText();
    };

};
module.exports = new page_softplan();
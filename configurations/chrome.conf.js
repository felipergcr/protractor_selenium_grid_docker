module.exports.config = require("../startTests")({
    capabilities: {
        browserName: "chrome",
        chromeOptions: {
            args: ["--headless"]
        }
    }
});
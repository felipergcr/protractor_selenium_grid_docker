module.exports.config = require("../startTests")({
    multiCapabilities: [
        {
            browserName: "firefox",
            "moz:firefoxOptions": {
                args: ["--headless"]
            }
        },
        {
            browserName: "chrome",
            chromeOptions: {
                args: ["--headless"]
            }
        }
    ]
});

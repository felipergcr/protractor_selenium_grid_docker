module.exports.config = require("../startTests")({
    capabilities: {
        browserName: "firefox",
        "moz:firefoxOptions": {
            args: ["--headless"]
        }
    }
});

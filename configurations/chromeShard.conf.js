module.exports.config = require("../startTests")({
    capabilities: {
        browserName: "chrome",
        chromeOptions: {
            args: ["--headless"]
        },
        shardTestFiles: true,
        maxInstances: 2
    }
});

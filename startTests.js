/**
 * Created by roque on 07/12/2018.
 */
const Jasmine2HtmlReporter = require('protractor-jasmine2-html-reporter');

const today = new Date(),
    timeStamp = today.getMonth() + 1 + '-' + today.getDate() + '-' + today.getFullYear() + '-' + today.getHours() + 'h-' + today.getMinutes() + 'm';

const reporter = new Jasmine2HtmlReporter({

     savePath: 'ReportTestResult/'
    ,showSummary: true
    ,showQuickLinks: true
    ,showConfiguration: true
    ,screenshotsFolder: 'Screenshots'
    ,takeScreenshots: true
    ,takeScreenshotsOnlyOnFailures: true
    ,fixedScreenshotName: true
    ,showPassed: true
    ,consolidate: true
    ,consolidateAll: false
    ,fileName: 'Report'
    ,fileNameSeparator: '_'
    ,fileNameDateSuffix: true
    ,fileNameSeparator: '_'
    ,cleanDestination: false
});


module.exports = providedConfig => {

    const defaultConfig = {
        seleniumAddress: "http://localhost:4444/wd/hub",
        specs: ["./tests/*.spec.js"],

        onPrepare: function() {
            browser.driver.manage().window().maximize();
            browser.ignoreSynchronization = true;
            browser.waitForAngular();
            jasmine.getEnv().addReporter(reporter);
            var Reporter = require('jasmine-spec-reporter').SpecReporter;
            jasmine.getEnv().addReporter(new Reporter({

                displayFailuresSummary: true // display summary of all failures after execution
                ,displayFailedSpec: true      // display each failed spec
                ,displaySuiteNumber: true    // display each suite number (hierarchical)
                ,displaySpecDuration: true   // display each spec duration
                ,colors: {
                    success: 'green'
                    ,failure: 'red'
                    ,pending: 'yellow'
                }
                ,prefixes: {
                    success: '✓ '
                    ,failure: '✗ '
                    ,pending: '* '
                }
            }));
        }
    };
    return Object.assign({}, defaultConfig, providedConfig);
};